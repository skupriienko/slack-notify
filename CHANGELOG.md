# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.0

- minor: Bump bitbucket-pipes-toolkit -> 2.2.0.

## 1.0.2

- patch: Make pretext for slack message configurable.

## 1.0.1

- patch: Update documentation for complex strings inside MESSAGE variable.

## 1.0.0

- major: Move pipe's source code to Python language.

## 0.3.7

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.3.6

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.5

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.4

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.2

- patch: Minor documentation updates

## 0.3.1

- patch: Refactor pipe code to use pipes bash toolkit.

## 0.3.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.4

- patch: Updated contributing guidelines

## 0.2.3

- patch: Fix string interpolation inside the jq command used to prepare the message sent to slack.

## 0.2.2

- patch: Fix escaping variables with double quotes.

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Add support for the DEBUG variable.
- minor: Switch naming from task to pipe. Add the pipe.yml descriptor.

## 0.1.1

- patch: Improve task logging

## 0.1.0

- minor: Initial version

